import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChaptersService {

  constructor(private http:HttpClient) { }
  getChapters():any {
    return this.http.get("https://rickandmortyapi.com/api/episode")
  }
  getChapterById(id:any):Observable<any>{
    return this.http.get("https://rickandmortyapi.com/api/episode/"+id)
  }
}
