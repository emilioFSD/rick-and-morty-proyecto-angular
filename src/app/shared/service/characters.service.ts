import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  constructor(private http:HttpClient) { }
  getCharacters():any{
    return this.http.get("https://rickandmortyapi.com/api/character")
  }
    
    getCharacterById(id:any):Observable<any>{
      return this.http.get("https://rickandmortyapi.com/api/character/"+id)
    }
    getCharactersByUrl(url:any):Observable<any>{
      return this.http.get(url)

    }
}
