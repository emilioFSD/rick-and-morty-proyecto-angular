import { LocationsService } from './../../../shared/service/locations.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-location-detail',
  templateUrl: './location-detail.component.html',
  styleUrls: ['./location-detail.component.scss']
})
export class LocationDetailComponent implements OnInit {
locationDetail:any = {}
  constructor(private route:ActivatedRoute, private LocationsService:LocationsService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((param) => {
      let idLocation = param.get('id')
      console.log(idLocation)
      this.LocationsService.getLocationsById(idLocation).subscribe( (location) => {
      this.locationDetail = location

      })
    })
  }

}
