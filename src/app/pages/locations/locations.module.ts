import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocationsRoutingModule } from './locations-routing.module';
import { LocationsComponent } from './locations.component';
import { LocationDetailComponent } from './location-detail/location-detail.component';
import { LocationGalleryComponent } from './location-gallery/location-gallery.component';


@NgModule({
  declarations: [
    LocationsComponent,
    LocationDetailComponent,
    LocationGalleryComponent
  ],
  imports: [
    CommonModule,
    LocationsRoutingModule
  ]
})
export class LocationsModule { }
