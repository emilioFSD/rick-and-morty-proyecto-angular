import { LocationsService } from './../../shared/service/locations.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {

  locationsList:any = []
  constructor(private LocationsService:LocationsService) { }

  ngOnInit(): void {
    this.LocationsService.getLocations().subscribe( (location:any) => {
    this.locationsList = location.results
     console.log(this.locationsList) 
    })
  
  }

}
