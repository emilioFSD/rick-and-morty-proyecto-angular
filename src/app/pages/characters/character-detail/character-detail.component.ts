import { CharactersService } from './../../../shared/service/characters.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.scss'],
})
export class CharacterDetailComponent implements OnInit {
  characterDetail: any = {};
  constructor(
    private route: ActivatedRoute,
    private CharactersService: CharactersService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((param) => {
      let idCharacter = param.get('id');
      console.log(idCharacter);
      this.CharactersService.getCharacterById(idCharacter).subscribe(
        (character) => {
          this.characterDetail = character;
        }
      );
    });
  }
}
