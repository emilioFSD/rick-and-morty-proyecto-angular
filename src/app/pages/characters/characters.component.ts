import { CharactersService } from './../../shared/service/characters.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {
  charactersList:any = []


  constructor(private CharactersService:CharactersService) { }

  ngOnInit(): void {
    this.CharactersService.getCharacters().subscribe( (character:any) => {
      this.charactersList = character.results
      console.log(this.charactersList)
    })
  }

}
