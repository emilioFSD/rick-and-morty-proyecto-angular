import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChapterGalleryComponent } from './chapter-gallery.component';

describe('ChapterGalleryComponent', () => {
  let component: ChapterGalleryComponent;
  let fixture: ComponentFixture<ChapterGalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChapterGalleryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChapterGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
