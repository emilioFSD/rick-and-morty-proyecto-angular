import { Component, Input, OnInit } from '@angular/core';
import { __importDefault } from 'tslib';

@Component({
  selector: 'app-chapter-gallery',
  templateUrl: './chapter-gallery.component.html',
  styleUrls: ['./chapter-gallery.component.scss']
})
export class ChapterGalleryComponent implements OnInit {
@Input () chaptersList:any = []
  constructor() { }

  ngOnInit(): void {
  }

}
