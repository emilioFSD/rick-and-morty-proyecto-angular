import { ChaptersService } from './../../shared/service/chapters.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chapters',
  templateUrl: './chapters.component.html',
  styleUrls: ['./chapters.component.scss']
})
export class ChaptersComponent implements OnInit {
chaptersList:any = []
  constructor(private ChaptersService:ChaptersService) { }

  ngOnInit(): void {
    this.ChaptersService.getChapters().subscribe( (chapter:any) => {
      this.chaptersList = chapter
      console.log(this.chaptersList)
  })
}
}

