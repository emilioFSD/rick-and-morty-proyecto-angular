import { ChaptersService } from './../../../shared/service/chapters.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chapter-detail',
  templateUrl: './chapter-detail.component.html',
  styleUrls: ['./chapter-detail.component.scss']
})
export class ChapterDetailComponent implements OnInit {
chapterDetail:any = {}
  constructor(private route:ActivatedRoute, private ChaptersService:ChaptersService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((param) => {
      let idChapter = param.get('id')
      // console.log(idChapter)
      this.ChaptersService.getChapterById(idChapter).subscribe( (chapter) => {
      this.chapterDetail = chapter.results
      
console.log(this.chapterDetail)
      })
    })
  }

}
