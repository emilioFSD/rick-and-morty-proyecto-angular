import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChaptersRoutingModule } from './chapters-routing.module';
import { ChaptersComponent } from './chapters.component';
import { ChapterDetailComponent } from './chapter-detail/chapter-detail.component';
import { ChapterGalleryComponent } from './chapter-gallery/chapter-gallery.component';


@NgModule({
  declarations: [
    ChaptersComponent,
    ChapterDetailComponent,
    ChapterGalleryComponent
  ],
  imports: [
    CommonModule,
    ChaptersRoutingModule
  ]
})
export class ChaptersModule { }
