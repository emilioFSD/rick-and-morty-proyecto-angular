import { ChapterDetailComponent } from './chapter-detail/chapter-detail.component';
import { ChaptersComponent } from './chapters.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ChaptersComponent,
  },
  { path: ':id', component: ChapterDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChaptersRoutingModule {}
