import { AboutComponent } from './pages/about/about.component';
import { ChaptersModule } from './pages/chapters/chapters.module';
import { LocationsModule } from './pages/locations/locations.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path: `home`, loadChildren: () =>
      import('./pages/home/home.module').then(m => m.HomeModule)
  },
  {
    path: `characters`, loadChildren: () =>
      import('./pages/characters/characters.module').then(m => m.CharactersModule)
  },
  {
    path: `locations`, loadChildren: () =>
      import('./pages/locations/locations.module').then(m => m.LocationsModule)
  },
  {
    path: `chapters`, loadChildren: () =>
      import('./pages/chapters/chapters.module').then(m => m.ChaptersModule)
  },
  {
    path: `about`, loadChildren: () =>
      import('./pages/about/about.module').then(m => m.AboutModule)
  },
  { path: ``, redirectTo: `home`, pathMatch: `full` 
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
